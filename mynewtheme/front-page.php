<?php get_header(); ?>
  <div class="row">
    <div class="col-sm-12 text-center p-5 bg-info text-white">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 p-4 text-justify">
      <?php the_field('presentation') ?>
    </div>
  </div>

  <div class="row d-flex justify-content-around align-items-center p-5">
    <div class="col-xl-3 col-l-3 col-sm-12 d-flex justify-content-center">
      <img class="img-fluid home-img" src="<?php the_field('image1_accueil') ?>" alt="">
    </div>
    <div class="col-xl-3 col-l-3 col-sm-12 d-flex justify-content-center">
      <img class="img-fluid home-img" src="<?php the_field('image2_accueil') ?>" alt="">
    </div>
    <div class="col-xl-3 col-l-3 col-sm-12 d-flex justify-content-center">
      <img class="img-fluid home-img" src="<?php the_field('image3_accueil') ?>" alt="">
    </div>
  </div>

<?php get_footer(); ?>
