<?php get_header(); ?>
<div class="row">
  <div class="col-sm-12 text-center p-5 bg-info text-white">
    <h1><?php the_title(); ?></h1>
  </div>
</div>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="row p-4 d-flex justify-content-center">
      <div class="col-sm-2 text-center auth">
        <span class="dashicons dashicons-admin-users mr-4"></span><?php the_author(); ?>
      </div>
      <div class="col-sm-2 text-center date">
        <span class="dashicons dashicons-calendar-alt mr-4"></span><?php the_time('d/m/Y'); ?>
      </div>
    </div>
    <div class="row d-flex justify-content-center">
      <img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="img post">
    </div>

    <p class="text-justify"><?php the_field('para1'); ?></p>
    <p class="text-justify"><?php the_field('para2'); ?></p>
  <?php endwhile; else: ?>
<?php endif; ?>


<?php get_footer(); ?>
