<?php

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );

register_nav_menu('header', 'entête du menu');
register_nav_menu('footer', 'Pied de page');

function mytheme_register_assets(){
  wp_register_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css');
  //attention, peut entrer en conflit si on veut installer des plgins
  wp_deregister_script('jquery');
  wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.slim.min.js', [], false, true);
  wp_register_script('popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', [], false, true);
  wp_register_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js', [], false, true);
  wp_enqueue_style( 'style', get_stylesheet_uri() );
  wp_enqueue_style('bootstrap');
  wp_enqueue_script('jquery');
  wp_enqueue_script('popper');
  wp_enqueue_script('bootstrap');
}

function ww_load_dashicons(){
    wp_enqueue_style('dashicons');
}



function custom_post_type() {
  // On enregistre notre custom post type qu'on nomme ici "bd" et ses arguments
  register_post_type( 'bd', [
    'label' => 'BD',
    'public' => true,
    'menu_icon' => 'dashicons-text-page',
    'menu_position' => 3,
    'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'categories' ),
    'rewrite' => array( 'slug' => 'BD'),
    'show_in_rest' => true,
    'has_archive' => true,
    'show_in_nav_menus' => true,
    'hierarchical'        => false,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_admin_bar'   => true,
    'can_export'          => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'taxonomies'  => array( 'category' ),
    ] );
  }

  //get_role pour faire la fonction de l'auteur

  function mytheme_menu_class($classes){
    $classes[] = 'nav-item';
    return $classes;
  }

  function mytheme_menu_link_class($attrs){
    $attrs['class'] = 'nav-link text-white';
    return $attrs;
  }

  function remove_author_publish_posts () {
    // $wp_roles est une instance de WP_Roles.
    global $wp_roles;
    $wp_roles->remove_cap( 'author' , 'publish_posts' );
 };

 function wporg_add_custom_post_types($query) {
     if ( is_author() && $query->is_main_query() ) {
         $query->set( 'post_type', array( 'post', 'page', 'bd' ) );
     }
     return $query;
 }


  add_action('pre_get_posts', 'wporg_add_custom_post_types');
  add_action( 'wp_enqueue_scripts', 'mytheme_register_assets');
  add_action('wp_enqueue_scripts', 'ww_load_dashicons');
  add_action( 'init', 'custom_post_type', 0 );
  add_action( 'init' , 'remove_author_publish_posts' );
  add_filter('nav_menu_css_class', 'mytheme_menu_class');
  add_filter('nav_menu_link_attributes', 'mytheme_menu_link_class');
  add_filter('pre_get_posts', 'query_post_type');
  function query_post_type($query) {
    if( is_category() ) {
      $post_type = get_query_var('post_type');
      if($post_type)
      $post_type = $post_type;
      else
      $post_type = array('nav_menu_item', 'post', 'bd'); // don't forget nav_menu_item to allow menus to work!
      $query->set('post_type',$post_type);
      return $query;
    }
  }



//fonction php générer pour ACF
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_612780888b36b',
	'title' => 'bd post',
	'fields' => array(
		array(
			'key' => 'field_612780dc35404',
			'label' => 'photo1 bd',
			'name' => 'photo1_bd',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_6127841f4f4ad',
			'label' => 'photo2 bd',
			'name' => 'photo2_bd',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_612784784f4ae',
			'label' => 'photo3 bd',
			'name' => 'photo3_bd',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_6127848b4f4af',
			'label' => 'photo4 bd',
			'name' => 'photo4_bd',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'bd',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_6128af75245ab',
	'title' => 'Page accueil',
	'fields' => array(
		array(
			'key' => 'field_6128af9be326f',
			'label' => 'Présentation',
			'name' => 'presentation',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_6128afbfe3270',
			'label' => 'Image1 accueil',
			'name' => 'image1_accueil',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_6128afeae3271',
			'label' => 'Image2_accueil',
			'name' => 'image2_accueil',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_6128b001e3272',
			'label' => 'image3_accueil',
			'name' => 'image3_accueil',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'page',
				'operator' => '==',
				'value' => '27',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_6143280d12dc2',
	'title' => 'structure article',
	'fields' => array(
		array(
			'key' => 'field_6143285693c8e',
			'label' => 'para1',
			'name' => 'para1',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_614328d993c8f',
			'label' => 'para2',
			'name' => 'para2',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;
