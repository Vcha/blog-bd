<?php get_header(); ?>
<div class="row">
  <div class="col-sm-12 text-center p-5 bg-info text-white">
    <h1><?php the_archive_title(); ?></h1>
  </div>
</div>
<div class="row d-flex justify-content-center">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="card col-xl-3 col-md-5 col-sm-12 m-4">
      <div class="card-img-top" style="height:200px; background-image: url('<?php the_post_thumbnail_url(); ?>'); background-size: cover; background-position: 50% 50%;">

      </div>
      <div class="card-body">
        <h5 class="card-title"><?php the_title(); ?></h5>
        <p class="card-text"><?php the_time('d/m/Y'); ?></p>
        <p class="card-text"><?php the_excerpt(); ?></p>
        <a href="<?php the_permalink(); ?>" class="btn btn-info">Voir plus...</a>
      </div>
    </div>
  <?php endwhile; else: ?>
</div>
  <h1>Pas d'article</h1>
<?php endif; ?>

<?php get_footer(); ?>
