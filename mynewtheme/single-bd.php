<?php get_header(); ?>
<div class="row">
  <div class="col-sm-12 text-center p-5 bg-info text-white">
    <h1><?php the_title(); ?></h1>
  </div>
</div>
<div class="row">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-sm-12 text-center p-3">
      <h2>Auteur : <?php the_author(); ?></h2>
    </div>
    <div class="col-sm-12 d-flex justify-content-center p-3">
      <img class="img-fluid" src="<?php the_field('photo1_bd'); ?>" />
    </div>
    <div class="col-sm-12 d-flex justify-content-center p-3">
      <img class="img-fluid" src="<?php the_field('photo2_bd'); ?>" />
    </div>
    <div class="col-sm-12 d-flex justify-content-center p-3">
      <img class="img-fluid" src="<?php the_field('photo3_bd'); ?>" />
    </div>
    <div class="col-sm-12 d-flex justify-content-center p-3">
      <img class="img-fluid" src="<?php the_field('photo4_bd'); ?>" />
    </div>
  <?php endwhile; else: ?>
<?php endif; ?>
</div>

<?php get_footer(); ?>
